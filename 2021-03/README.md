# Convocatoria Marzo 2021

El siguiente listado corresponde a los aspirantes:

* [Christian Rodriguez](aspirante-rodriguez.christian/)

* [Scavuzzo Nathalie Pabla](aspirante-scavuzzo.nathalie)

* [Marco Cristofano](aspirante-cristofano.marco/)

* [Ariadna Aspitia](aspirante-aspitia.ariadna/)

* [Manuel Kloster](aspirante-kloster.manuel/)