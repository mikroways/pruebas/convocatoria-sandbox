# Valentín Cañete

## Resumen
Analista en TIC, egresado de la [Facultad de
Informática](https://www.info.unlp.edu.ar/), [Universidad
Nacional de La Plata](http://unlp.edu.ar/). Fui ayudante alumno de Redes de Datos 2
en la Facultad de Informática de la UNLP y Sistemas Operativos y Redes en la Facultad de Ingeniería de la UNLP. Además estoy por graduarme de Ingeniero en Computación en la Facultad Virtual de Ingeniería e Informática de la UNLP.

## Antecedentes

En el [siguiente enlace](https://valentinca.gitlab.io/my-cv) puede verse su CV online.


![Valentín](./valen.jpg)


